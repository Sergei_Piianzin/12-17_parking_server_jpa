var pagination = {}

pagination.spaceCount = 0;
pagination.itemsOnPage = 10;
pagination.pageCount = 1;
pagination.activePage = 1;

pagination.createPagingButtons = function() {
	goAjax(function(data) {
		var pages = pagination.getPageCount(data);
		var elem = document.getElementById("pagination-ul");
		elem.innerHtml = "";
		for (var i = 1; i <= pages; i++) {
			var classList;
			if (i === pagination.activePage) {
				classList = "pagination-button active";
			} else {
				classList = "pagination-button";
			}
			elem.innerHTML += '<li><a class="'+classList+'">' + i
			+ '</a></li>';
		}
		pagination.bindPaging();
	});
}

pagination.setActive = function(page) {
	var elems = document.getElementsByClassName("pagination-button");
	for(var i=0; i < elems.length; ++i ) {
		if ((i) == page-1) {
			elems[i].className = "pagination-button active";
		} else {
			elems[i].className = "pagination-button";
		}
	}
}

pagination.bindPaging = function() {
	var elems = document.getElementsByClassName('pagination-button');
	[].forEach.call(elems, function(button) {
		button.addEventListener("click", function(event) {
			var elem = event.target;
			var page = elem.childNodes[0].data;
			var count = pagination.itemsOnPage;
			application.controller.getParking([count, page]);
			pagination.setActive(page);
		});
	});
}

pagination.getPageCount = function(data) {
	pagination.spaceCount = data.count;
	var count = pagination.spaceCount / pagination.itemsOnPage;
	if (count > this.pageCount) {
		return count.toFixed();
	}
	return pagination.pageCount;
}

function goAjax(callback) {
	$.ajax({url:'/service/spaces/count', success: function(data) {
		callback(data);
	}});
}
