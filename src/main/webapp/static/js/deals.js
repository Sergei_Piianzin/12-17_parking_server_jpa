var deals = {}

deals.generateDeals = function(data) {
	var elem = document.getElementsByClassName("content-block");
	elem[0].innerHTML = "";
	var rows = "<table class=\"deals-table\"><tr><td>id</td><td>Client</td><td>Parking number</td><td>Employee</td></tr>";
	for (var i = 0; i < data.length; ++i) {
		rows += '<tr><td>' + data[i].id + '</td><td>'
				+ data[i].client.first_name + ' ' + data[i].client.last_name
				+ '</td><td>' + data[i].parking.number + '</td><td>'
				+ data[i].employee.first_name + ' '
				+ data[i].employee.last_name
				+ '</td><td class="deals-table-delete">delete</td></tr>';
	}
	rows += "</table>";
	elem[0].innerHTML = rows;
}

deals.getDealsByAjax = function(count, callback) {
	$.ajax({
		url : '/service/deals?count=' + count,
		success : function(data) {
			callback(data);
		}
	});

}

deals.showDeals = function() {
	deals.getDealsByAjax(10, function(data) {
		deals.generateDeals(data);
		deals.bindButtons();
	});
}

deals.bindAdminButton = function() {
	var elem = document.getElementById('dealsEdit');
	elem.addEventListener("click", function(event) {
		deals.showDeals();
	});
}

deals.bindButtons = function() {
	elems = document.getElementsByClassName("deals-table-delete");
	[].forEach.call(elems, function(dealButton) {
		dealButton.addEventListener("click", function(event) {
			var elem = event.target.parentNode.childNodes[0];
			var text = elem.textContent || elem.innerText;
			$.ajax({
				url : '/service/deal/' + text + '/delete',
				success : function() {
					deals.showDeals();
				},
				error : function() {
					alert('Something wrong. Try it later');
				}
			})
		})
	})
}
