<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<title>Parking</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/css/style.css" />
<link rel="stylesheet" type="text/css" href="/css/animation.css" />
<link rel="stylesheet" type="text/css" href="/css/buttons.css" />
<link rel="stylesheet" type="text/css" href="/css/popup.css" />
</head>
<body>
	<div class="background-blured-img"></div>
	<div class="centred">
		<div class="background-whiteplate"></div>
		<div class="header">
			<img src="/img/banner.jpg">
			<ul>
				<li id="button1" class="button-menu">Parking spaces</li>
				<li id="button2" class="button-menu">Managers</li>
				<li id="button3" class="button-menu">About us</li>
			</ul>
		</div>
		<div class="content-block">
			<div>
				<form:form id="form" class="login-form" method="post"
					commandName="loginForm" action="/login">
					<h3 id="title">
						<c:if test="${msg != null}">
							<p>${msg}</p>
						</c:if>
					</h3>
					<p>
						<label>Login:</label>
						<form:input type="text" id="login" name="login" path="login" />
						<form:errors path="login" cssClass="error" />
					</p>
					<p>
						<label>Password:</label>
						<form:input type="password" id="password" name="password"
							path="password" />
						<form:errors path="password" cssClass="error" />
					</p>
					<p>
						<input type="checkbox" name="remember-me">Remember me</input>
					</p>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<button class="submit-ok" type="submit" class="btn">Signup</button>
					<button formaction="/registration" formmethod="get" class="submit-reg" type="submit">Registration</button>
					<form:errors path="error" cssClass="error" />
				</form:form>
			</div>
		</div>
	</div>
	<div id="footer" class="centred"></div>
</body>
</html>