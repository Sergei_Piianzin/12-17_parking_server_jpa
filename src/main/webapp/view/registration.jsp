<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<title>Parking</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/css/style.css" />
<link rel="stylesheet" type="text/css" href="/css/animation.css" />
<link rel="stylesheet" type="text/css" href="/css/buttons.css" />
<link rel="stylesheet" type="text/css" href="/css/popup.css" />
</head>
<body>
	<div class="background-blured-img"></div>
	<div class="centred">
		<div class="background-whiteplate"></div>
		<div class="header">
			<img src="/img/banner.jpg">
			<ul>
				<li id="button1" class="button-menu">Parking spaces</li>
				<li id="button2" class="button-menu">Managers</li>
				<li id="button3" class="button-menu">About us</li>
			</ul>
		</div>
		<div class="content-block">
			<div>
				<form:form id="form1" class="login-form" method="post"
					action="/registration" commandName="registrationForm">
					<h3 id="title"><form:errors path="error" cssClass="error" /></h3>
					<p>
						<label>Login:</label> <form:input path="login" type="text" name="login" />
						<form:errors path="login" cssClass="error" />
					</p>
					<p>
						<label>Password:</label> <form:input path="password" type="text" name="password"/>
						<form:errors path="password" cssClass="error" />
					</p>
					<p>
						<label>First name:</label> <form:input path="firstName" type="text" name="first_name"/>
						<form:errors path="firstName" cssClass="error" />
					</p>
					<p>
						<label>Second name:</label> <form:input path="lastName" type="text" name="last_name"/>
						<form:errors path="lastName" cssClass="error" />
					</p>
					<p>
						<label>Phone:</label> <form:input path="phone" type="text" name="phone"/>
						<form:errors path="phone" cssClass="error"/>
					</p>
					<p class=login-form-centred>
						<input type="submit" name="registartion_form" value="Send" />
					</p>
				</form:form>
			</div>
		</div>
	</div>
	<div id="footer" class="centred"></div>
</body>
</html>