<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>
<head>
<title>Parking</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/css/style.css" />
<link rel="stylesheet" type="text/css" href="/css/animation.css" />
<link rel="stylesheet" type="text/css" href="/css/buttons.css" />
<link rel="stylesheet" type="text/css" href="/css/popup.css" />
</head>
<body>
	<div class="background-blured-img"></div>
	<div class="centred">
		<div class="background-whiteplate"></div>
		<div class="header">
			<img src="/img/banner.jpg">
			<ul>
				<li id="button1" class="button-menu">Parking spaces</li>
				<li id="button2" class="button-menu">Managers</li>
				<li id="button3" class="button-menu">About us</li>
			</ul>
		</div>
		<div class="login">
			<sec:authorize access="hasRole('CHIEF_MANAGER')">
				<p class="login-p">
				<div class="button-logout" id="dealsEdit">Deals</div>
				</p>
			</sec:authorize>
			<p class="login-p">
				<a href="/logout">
					<div id="button-logout" class="button-logout">Logout</div>
				</a>
			</p>
		</div>
		<div class="content-block">
			<div id="parkingBlock" class="content">
				<div id="parkingList"></div>
				<div class=pagination-bar>
					<ul id="pagination-ul" class="pagination"></ul>
				</div>
			</div>
			<div id="employeeBlock" class="content"></div>
			<div id="letterBlock" class="content"></div>
		</div>
		<div id="footer" class="centred">
			<img src="/img/footer.jpg">
		</div>
	</div>

	<!--Hidden elements -->
	<div id="popup" class="b-popup">
		<div class="b-popup-content">
			<!--close-button -->
			<div class="cross"></div>
			<form id="form1" method="post">
				<h3 id="title"></h3>
				<p>
					<label>Car number:</label> <input type="text" name="car_number"></input>
				</p>
				<p>
					<label>Manufacter:</label> <input type="text" name="manufacter"></input>
				</p>
				<p>
					<label>Model:</label> <input type="text" name="vodel"></input>
				</p>
				<p>
					<label>Body type:</label> <input type="text" name="bodytype"></input>
				</p>
				<p>
					<label>Date:</label> <input type="datetime-local" name="bdaytime"></input>
				</p>
				<p>
					<input type="submit" name="submit" />
				</p>
			</form>
			<!--popup-result -->
			<div id="results"></div>
		</div>
	</div>
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/parking.js"></script>
	<script type="text/javascript" src="/js/pagination.js"></script>
	<sec:authorize access="hasRole('CHIEF_MANAGER')">
		<script type="text/javascript" src="/js/deals.js"></script>
	</sec:authorize>
</body>
</html>