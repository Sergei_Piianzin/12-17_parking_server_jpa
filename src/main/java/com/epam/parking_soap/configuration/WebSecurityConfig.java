package com.epam.parking_soap.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;

import com.epam.parking_soap.security.Role;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private UserDetailsService userDetailsService;

  @Autowired
  private PasswordEncoder encoder;

  @Autowired
  private JdbcTokenRepositoryImpl tokenRepository;

  @Autowired
  public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(encoder);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable().authorizeRequests().antMatchers("/parking*")
        .hasRole(Role.REGISTERED_USER.toString()).antMatchers("/service/deal/*/delete")
        .hasRole(Role.CHIEF_MANAGER.toString())
        .antMatchers("/css*", "/img*", "/js*", "/registration", "/login*", "/service*").permitAll()
        .and().formLogin().loginPage("/login").usernameParameter("login")
        .defaultSuccessUrl("/parking").and().rememberMe().tokenRepository(tokenRepository)
        .tokenValiditySeconds(600);

    http.logout().logoutUrl("/logout").logoutSuccessUrl("/login.end").invalidateHttpSession(true);
  }
}
