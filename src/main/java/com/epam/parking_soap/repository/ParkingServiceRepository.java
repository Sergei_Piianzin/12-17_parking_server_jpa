package com.epam.parking_soap.repository;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.parking_soap.repository.dao.ClientDao;
import com.epam.parking_soap.repository.dao.DealDao;
import com.epam.parking_soap.repository.dao.EmployeeDao;
import com.epam.parking_soap.repository.dao.ParkingSpaceDao;

@Repository
public class ParkingServiceRepository {
  Logger LOG = org.slf4j.LoggerFactory.getLogger(ParkingServiceRepository.class);

  @Autowired
  private ClientDao clientDao;
  @Autowired
  private DealDao dealDao;
  @Autowired
  private EmployeeDao employeeDao;
  @Autowired
  private ParkingSpaceDao parkingDao;

  public ParkingServiceRepository() {
    LOG.debug("Create instance of ParkingServiceRepository class");
  }

  public ParkingSpaceDao getparkingDao() {
    return parkingDao;
  }

  public ClientDao getClientDao() {
    return clientDao;
  }

  public DealDao getDealDao() {
    return dealDao;
  }

  public EmployeeDao getEmployeeDao() {
    return employeeDao;
  }
}
