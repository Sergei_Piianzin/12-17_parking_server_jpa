package com.epam.parking_soap.repository.dao.entity;

import javax.persistence.*;

@Entity
@Table(name = "\"Position\"")
public class Position {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private String title;
  private int salary;

  public Position() {}

  public Position(String title, int salary) {
    super();
    this.title = title;
    this.salary = salary;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getSalary() {
    return salary;
  }

  public void setSalary(int salary) {
    this.salary = salary;
  }

  @Override
  public String toString() {
    return "Position [title=" + title + ", salary=" + salary + "]";
  }

}
