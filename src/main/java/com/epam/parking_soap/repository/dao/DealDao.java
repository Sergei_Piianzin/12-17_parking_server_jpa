package com.epam.parking_soap.repository.dao;

import java.util.List;

import com.epam.parking_soap.repository.dao.entity.Deal;
import com.epam.parking_soap.repository.dao.entity.Deal_details;

public interface DealDao {
  void insertDeal(Deal deal);

  List<Deal> selectAllDeals();

  List<Deal> selectDeals(int count);

  Deal selectDealById(int id);

  Deal_details selectDealDetailsByDeal(Deal deal);

  void deleteDealById(int id);
}
