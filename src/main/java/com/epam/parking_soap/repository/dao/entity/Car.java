package com.epam.parking_soap.repository.dao.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "\"Car\"")
public class Car implements Serializable {
  @Transient
  private static final long serialVersionUID = -7937759211894091426L;

  @Id
  @Column
  private String number;

  @ManyToOne
  private Model model;

  public Car() {}

  public Car(String number, Model model) {
    this.number = number;
    this.model = model;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  @Override
  public String toString() {
    return "Car [number=" + number + ", model=" + model + "]";
  }

  public Model getModel() {
    return model;
  }

  public void setModel(Model model) {
    this.model = model;
  }
}
