package com.epam.parking_soap.repository.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.parking_soap.repository.dao.DealDao;
import com.epam.parking_soap.repository.dao.entity.Deal;
import com.epam.parking_soap.repository.dao.entity.Deal_details;

@Repository
public class DealDaoImpl implements DealDao {

  @PersistenceContext
  private EntityManager em;

  @Transactional
  public void insertDeal(Deal deal) {
    em.persist(deal.getClient().getCar());
    em.persist(deal.getClient());
    em.persist(deal);
  }

  public Deal selectDealById(int id) {
    return em.find(Deal.class, id);
  }

  @Override
  public List<Deal> selectAllDeals() {
    return em.createQuery("from Deal", Deal.class).getResultList();
  }

  @Override
  public Deal_details selectDealDetailsByDeal(Deal deal) {
    return em.find(Deal.class, deal.getId()).getDetails();
  }

  @Override
  public List<Deal> selectDeals(int count) {
    return em.createQuery("from Deal", Deal.class).setMaxResults(count).getResultList();
  }

  @Override
  @Transactional
  public void deleteDealById(int id) {
    Deal deal = selectDealById(id);
    if (deal != null) {
      em.remove(deal);
    }
  }

}
