package com.epam.parking_soap.repository.dao;

import java.util.List;

import com.epam.parking_soap.repository.dao.entity.Car;
import com.epam.parking_soap.repository.dao.entity.Client;

public interface ClientDao {
  List<Client> selectAllClients();

  Client selectClientByLogin(String login);

  Car selectCarByClient(Client client);

  void insertClient(Client client);

  boolean isClientExist(String login);

}
