package com.epam.parking_soap.repository.dao.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "\"Model\"")
public class Model implements Serializable {
  @Transient
  private static final long serialVersionUID = -3355941177835274088L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private String body_type;
  private String manufacter;
  private String model;

  public Model() {}

  public Model(int id, String body_type, String manufacter, String model) {
    super();
    this.id = id;
    this.body_type = body_type;
    this.manufacter = manufacter;
    this.model = model;
  }

  public Model(String body_type, String manufacter, String model) {
    super();
    this.body_type = body_type;
    this.manufacter = manufacter;
    this.model = model;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getBody_type() {
    return body_type;
  }

  public void setBody_type(String body_type) {
    this.body_type = body_type;
  }

  public String getManufacter() {
    return manufacter;
  }

  public void setManufacter(String manufacter) {
    this.manufacter = manufacter;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  @Override
  public String toString() {
    return "Model [id=" + id + ", body_type=" + body_type + ", manufacter=" + manufacter
        + ", model=" + model + "]";
  }

}
