package com.epam.parking_soap.repository.dao.entity;

import javax.persistence.*;

@Entity
@Table(name = "\"Employee\"")
public class Employee {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private String first_name;
  private String last_name;

  @ManyToOne
  @JoinColumn(name = "position")
  private Position position;

  public Employee() {

  }

  public Employee(int id, String first_name, String last_name, Position position) {
    super();
    this.id = id;
    this.first_name = first_name;
    this.last_name = last_name;
    this.position = position;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFirst_name() {
    return first_name;
  }

  public void setFirst_name(String first_name) {
    this.first_name = first_name;
  }

  public String getLast_name() {
    return last_name;
  }

  public void setLast_name(String last_name) {
    this.last_name = last_name;
  }

  public Position getPosition() {
    return position;
  }

  public void setPosition(Position position) {
    this.position = position;
  }

  @Override
  public String toString() {
    return "Employee [id=" + id + ", first_name=" + first_name + ", last_name=" + last_name
        + ", position=" + position + "]";
  }

}
