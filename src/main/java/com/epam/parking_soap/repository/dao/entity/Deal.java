package com.epam.parking_soap.repository.dao.entity;

import javax.persistence.*;

@Entity
@Table(name = "\"Deal\"")
public class Deal {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "client_id")
  private Client client;
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "parking_number")
  private Parking_space parking;
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "employee_id")
  private Employee employee;
  @Transient
  private Deal_details details;

  public Deal() {

  }

  public Deal(int id, Client client, Parking_space parking, Employee employee) {
    super();
    this.id = id;
    this.client = client;
    this.parking = parking;
    this.employee = employee;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Client getClient() {
    return client;
  }

  public void setClient(Client client) {
    this.client = client;
  }

  public Parking_space getParking() {
    return parking;
  }

  public void setParking(Parking_space parking) {
    this.parking = parking;
  }

  public Employee getEmployee() {
    return employee;
  }

  public void setEmployee(Employee employee) {
    this.employee = employee;
  }

  @Override
  public String toString() {
    return "Deal [id=" + id + ", client=" + client + ", parking=" + parking + ", employee="
        + employee + ", details=" + details + "]";
  }

  public Deal_details getDetails() {
    return details;
  }

  public void setDetails(Deal_details details) {
    this.details = details;
  }
}
