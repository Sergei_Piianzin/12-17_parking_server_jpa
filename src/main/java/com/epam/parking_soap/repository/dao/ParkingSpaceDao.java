package com.epam.parking_soap.repository.dao;

import java.util.List;

import com.epam.parking_soap.repository.dao.entity.Parking_space;

public interface ParkingSpaceDao {
  List<Parking_space> selectAllParking();

  Parking_space selectParkingById(long id);

  List<Parking_space> selectParking(int count, int offset);

  long countSpaces();
}
