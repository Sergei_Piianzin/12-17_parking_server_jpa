package com.epam.parking_soap.repository.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.epam.parking_soap.repository.dao.ParkingSpaceDao;
import com.epam.parking_soap.repository.dao.entity.Parking_space;

@Repository
public class ParkingSpaceDaoImpl implements ParkingSpaceDao {

  @PersistenceContext
  EntityManager em;

  public List<Parking_space> selectAllParking() {
    return em.createQuery("from Parking_space", Parking_space.class).getResultList();
  }

  public Parking_space selectParkingById(long id) {
    return em.find(Parking_space.class, id);
  }

  @Override
  @Cacheable
  public List<Parking_space> selectParking(int count, int offset) {
    return em.createQuery("from Parking_space", Parking_space.class).setFirstResult(offset)
        .setMaxResults(count).getResultList();
  }

  @Override
  public long countSpaces() {
    return em.createQuery("select count(*) from Parking_space", Long.class).getSingleResult();
  }


}
