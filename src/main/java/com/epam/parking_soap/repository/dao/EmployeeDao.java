package com.epam.parking_soap.repository.dao;

import java.util.List;

import com.epam.parking_soap.repository.dao.entity.Client;
import com.epam.parking_soap.repository.dao.entity.Employee;

public interface EmployeeDao {
  List<Employee> selectEmployeeByClient(Client client);

  Employee selectEmployeeById(long id);

  void insertEmployee(Employee employee);
}
