package com.epam.parking_soap.repository.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.parking_soap.repository.dao.EmployeeDao;
import com.epam.parking_soap.repository.dao.entity.Client;
import com.epam.parking_soap.repository.dao.entity.Employee;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

  @PersistenceContext
  private EntityManager em;

  public Employee selectEmployeeById(long id) {
    return em.find(Employee.class, id);
  }

  public List<Employee> selectEmployeeByClient(Client client) {
    return em.createQuery("from Employee", Employee.class).getResultList();
  }

  @Transactional
  public void insertEmployee(Employee employee) {
    em.persist(employee);
  }
}
