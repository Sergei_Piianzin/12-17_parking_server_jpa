package com.epam.parking_soap.repository.dao.entity;

import javax.persistence.*;

@Entity
@Table(name = "\"Parking_space\"")
public class Parking_space {
  @Id
  private int number;
  private String type;

  public Parking_space() {}

  public Parking_space(int nubmer, String type) {
    this.number = nubmer;
    this.type = type;
  }

  public long getNumber() {
    return number;
  }

  public void setNumber(int nubmer) {
    this.number = nubmer;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "Parking_space [nubmer=" + number + ", type=" + type + "]";
  }

}
