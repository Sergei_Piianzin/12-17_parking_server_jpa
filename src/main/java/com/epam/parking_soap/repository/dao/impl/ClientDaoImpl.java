package com.epam.parking_soap.repository.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.parking_soap.repository.dao.ClientDao;
import com.epam.parking_soap.repository.dao.entity.Car;
import com.epam.parking_soap.repository.dao.entity.Client;

@Repository
public class ClientDaoImpl implements ClientDao {

  Logger LOG = org.slf4j.LoggerFactory.getLogger(ClientDaoImpl.class);

  @PersistenceContext
  private EntityManager em;

  public ClientDaoImpl() {
    LOG.debug("Create instance of ClientDaoImpl class.");
  }

  public List<Client> selectAllClients() {
    return em.createQuery("from Client", Client.class).getResultList();
  }

  public Car selectCarByClient(Client client) {
    return em.find(Client.class, client.getId()).getCar();
  }

  @Transactional
  public void insertClient(Client client) {
    em.persist(client);
  }

  public boolean isClientExist(String login) {
    try {
      em.createQuery("from Client c where c.login=:login", Client.class)
          .setParameter("login", login).getSingleResult();
      return true;
    } catch (NoResultException e) {
      return false;
    }
  }

  public Client selectClientByLogin(String login) {
    return em.createQuery("from Client c where c.login=:login", Client.class)
        .setParameter("login", login).getSingleResult();
  }
}
