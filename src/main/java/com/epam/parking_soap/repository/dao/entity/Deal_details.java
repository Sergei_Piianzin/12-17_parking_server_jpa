package com.epam.parking_soap.repository.dao.entity;

import java.time.LocalDateTime;

import javax.persistence.*;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "\"Deal_details\"")
public class Deal_details {

  @Id
  private int deal_id;
  private int booking_hours;
  @Type(type = "timestamp")
  private LocalDateTime begining_date;

  public Deal_details() {}

  public Deal_details(int deal_id, int booking_hours, LocalDateTime begining_date) {
    super();
    this.deal_id = deal_id;
    this.booking_hours = booking_hours;
    this.begining_date = begining_date;
  }

  public int getDeal_id() {
    return deal_id;
  }

  public void setDeal_id(int deal_id) {
    this.deal_id = deal_id;
  }

  public int getBooking_hours() {
    return booking_hours;
  }

  public void setBooking_hours(int booking_hours) {
    this.booking_hours = booking_hours;
  }

  @Override
  public String toString() {
    return "Deal_details [deal_id=" + deal_id + ", booking_hours=" + booking_hours
        + ", LocalDateTime=" + begining_date + "]";
  }

  public LocalDateTime getBegining_date() {
    return begining_date;
  }

  public void setBegining_date(LocalDateTime begining_date) {
    this.begining_date = begining_date;
  }



}
