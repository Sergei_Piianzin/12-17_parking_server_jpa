package com.epam.parking_soap.repository.dao.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "\"Client\"")
public class Client implements Serializable {
  @Transient
  private static final long serialVersionUID = -2480931122929130732L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private String first_name;
  private String last_name;
  private String phone;
  private String login;
  private String password;
  @OneToOne
  private Car car;
  private Boolean chief;

  public Client() {}

  public Client(int id, String first_name, String last_name, String phone, Car car) {
    this.id = id;
    this.first_name = first_name;
    this.last_name = last_name;
    this.phone = phone;
    this.car = car;
  }

  public Client(String first_name, String last_name, String phone, String login, String password,
      Car car) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.phone = phone;
    this.login = login;
    this.password = password;
    this.car = car;
  }

  public Client(String first_name, String last_name, String phone, String login, String password) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.phone = phone;
    this.login = login;
    this.password = password;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFirst_name() {
    return first_name;
  }

  public void setFirst_name(String first_name) {
    this.first_name = first_name;
  }

  public String getLast_name() {
    return last_name;
  }

  public void setLast_name(String last_name) {
    this.last_name = last_name;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Car getCar() {
    return car;
  }

  public void setCar(Car car) {
    this.car = car;
  }

  @Override
  public String toString() {
    return "Client [id=" + id + ", " + first_name + " " + last_name + ", phone=" + phone
        + ", car_number=" + car + "]\n";
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Boolean isChief() {
    return chief;
  }

  public void setChief(Boolean chief) {
    this.chief = chief;
  }
}
