package com.epam.parking_soap.repository.dao.entity;

import javax.persistence.*;

@Entity
@Table(name = "\"Parking_type\"")
public class Parking_type {
  @Id
  private String type;
  private double count_per_hour;

  public Parking_type() {}

  public Parking_type(String type, double count_per_hour) {
    this.type = type;
    this.count_per_hour = count_per_hour;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public double getCount_per_hour() {
    return count_per_hour;
  }

  public void setCount_per_hour(double count_per_hour) {
    this.count_per_hour = count_per_hour;
  }

}
