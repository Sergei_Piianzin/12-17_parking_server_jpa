package com.epam.parking_soap.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.parking_soap.repository.ParkingServiceRepository;
import com.epam.parking_soap.repository.dao.entity.Client;
import com.epam.parking_soap.repository.dao.entity.Deal;
import com.epam.parking_soap.repository.dao.entity.Employee;
import com.epam.parking_soap.repository.dao.entity.Parking_space;


@RestController
@RequestMapping("/service")
public class RestParkingServiceImpl implements RestParkingService {
  Logger LOG = org.slf4j.LoggerFactory.getLogger(RestParkingServiceImpl.class);

  @Autowired
  private ParkingServiceRepository repository;

  public RestParkingServiceImpl() {
    LOG.debug("Create instan�e of RestParkingServiceImpl class.");
  }

  @Override
  public void addDeal(Client client, long employee_id, long parking_space_id) {
    Employee employee = repository.getEmployeeDao().selectEmployeeById(employee_id);
    Parking_space parking = repository.getparkingDao().selectParkingById(parking_space_id);
    if (employee == null) {
      throw new AddDealError("Employee does'n exist");
    }
    if (parking == null) {
      throw new AddDealError("Parking space does'n exist");
    }
    client.getCar().getModel().setId(4);
    Deal deal = new Deal();
    deal.setClient(client);
    deal.setEmployee(employee);
    deal.setParking(parking);
    repository.getDealDao().insertDeal(deal);
  }

  private static class AddDealError extends Error {
    private static final long serialVersionUID = 1L;

    public AddDealError(String msg) {
      super(msg);
    }
  }

  @RequestMapping("/deals")
  public List<Deal> getDeals(@RequestParam int count) {
    if (count < 1) {
      return new ArrayList<>();
    }
    return repository.getDealDao().selectDeals(count);
  }

  @RequestMapping("/client/{login}")
  public Client getClientByLogin(@PathVariable String login) {
    if (login == null || login.isEmpty()) {
      return null;
    }
    return repository.getClientDao().selectClientByLogin(login);
  }

  @RequestMapping(path = "/spaces", method = RequestMethod.GET)
  public List<Parking_space> getParkingSpaces(@RequestParam(name = "elems") int count,
      @RequestParam int page) {
    if (count > 0 && page >= 0) {
      int offset = page * count;
      return repository.getparkingDao().selectParking(count, offset);
    }
    return null;
  }

  @RequestMapping(path = "/client", method = RequestMethod.PUT)
  public void addClient(@RequestBody Client client) {
    if (client == null)
      return;
    LOG.debug("Client: {}", client);
    repository.getClientDao().insertClient(client);
  }

  @RequestMapping("/client/{login}/exist")
  public Boolean isClientExist(@PathVariable String login) {
    return repository.getClientDao().isClientExist(login);
  }

  @RequestMapping(path = "/spaces/count", method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public Map<String, Long> getParkingCount() {
    long count = repository.getparkingDao().countSpaces();
    LOG.debug("Parking count:{}", count);
    Map<String, Long> pair = new HashMap<>();
    pair.put("count", count);
    return pair;
  }

  @RequestMapping("/deal/{id}/delete")
  public void removeDeal(@PathVariable int id) {
    repository.getDealDao().deleteDealById(id);
  }
}
