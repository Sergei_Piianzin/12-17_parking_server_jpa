package com.epam.parking_soap.service;

import java.util.List;
import java.util.Map;

import com.epam.parking_soap.repository.dao.entity.Client;
import com.epam.parking_soap.repository.dao.entity.Deal;
import com.epam.parking_soap.repository.dao.entity.Parking_space;

public interface RestParkingService {

  void addDeal(Client client, long employee_id, long parking_space_id);

  Client getClientByLogin(String login);

  Map<String, Long> getParkingCount();

  void addClient(Client client);

  List<Parking_space> getParkingSpaces(int count, int page);

  Boolean isClientExist(String login);

  List<Deal> getDeals(int count);
}
