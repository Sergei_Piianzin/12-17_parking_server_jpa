package com.epam.parking_soap.security;

public enum Role {
  REGISTERED_USER, CHIEF_MANAGER
}
