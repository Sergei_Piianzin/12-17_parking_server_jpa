package com.epam.parking_soap.security;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.client.RestTemplate;

import com.epam.parking_soap.repository.dao.entity.Client;
import com.epam.parking_soap.utils.RestPaths;

public class UserDetailsServiceImpl implements UserDetailsService {
  @Autowired
  RestTemplate restTemplate;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Client client = restTemplate.getForObject(RestPaths.GET_CLIENT, Client.class, username);
    
    Set<GrantedAuthority> roles = new HashSet<>();
    roles.add(new SimpleGrantedAuthority("ROLE_" + Role.REGISTERED_USER.toString()));
    
    if (client.isChief() != null && client.isChief()) {
      roles.add(new SimpleGrantedAuthority("ROLE_" + Role.CHIEF_MANAGER.toString()));
    }
    
    UserDetails userDetails = new User(client.getLogin(), client.getPassword(), roles);
    return userDetails;
  }

}
