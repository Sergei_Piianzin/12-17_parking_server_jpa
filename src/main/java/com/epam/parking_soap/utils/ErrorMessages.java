package com.epam.parking_soap.utils;

public class ErrorMessages {
  public static final String ALREADY_EXIST = "User already exists.";
  public static final String DOESNT_EXIST = "User doesn't exists.";
  public static final String EMPTY_FIELDS = "All fields must be filled.";
  public static final String EMPTY_LOGIN = "Login is empty.";
  public static final String EMPTY_PASSWORD = "Password is empty.";
  public static final String WRONG_AUTH = "Login or password is wrong.";
}
