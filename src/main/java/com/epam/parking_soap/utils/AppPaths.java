package com.epam.parking_soap.utils;

public class AppPaths {
  public static final String REGISTRATION = "registration";
  public static final String REGISTRATION_URL = "/registration";
  public static final String REGISTRATION_SEND = "/registration/send";
  public static final String PARKING = "parking";
  public static final String PARKING_DEFAULT_URL = "/parking";
  public static final String LOGIN = "login";
  public static final String LOGIN_URL = "/login";
  public static final String LOGIN_END = "/login.end";
  public static final String ERROR_URL = "/error";
  public static final String ERROR = "error";
}
