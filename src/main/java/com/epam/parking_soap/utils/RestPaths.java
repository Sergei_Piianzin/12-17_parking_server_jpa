package com.epam.parking_soap.utils;

public class RestPaths {
  public static final String ROOT = "http://localhost:8080";
  public static final String GET_CLIENT = ROOT + "/service/client/{login}";
  public static final String GET_CLIENT_EXIST = ROOT + "/service/client/{login}/exist";
  public static final String GET_SPACES = ROOT + "/service/spaces";
  public static final String PUT_CLIENT = ROOT + "/service/client";
}
