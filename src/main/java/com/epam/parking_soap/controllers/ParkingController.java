package com.epam.parking_soap.controllers;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.parking_soap.utils.AppPaths;

@Controller
@Scope("session")
public class ParkingController {
  Logger LOG = org.slf4j.LoggerFactory.getLogger(ParkingController.class);

  public ParkingController() {
    LOG.debug("Create an instance of  ParkingController class.");
  }

  @RequestMapping(path = {AppPaths.PARKING_DEFAULT_URL, "/"})
  public String getParking(HttpSession session) {
    return AppPaths.PARKING;
  }
}
