package com.epam.parking_soap.controllers.forms;

import javax.validation.constraints.Size;

public class LoginForm {
  @Size(min=1, message="Login is empty")
  private String login;
  @Size(min=1, message="Password is empty")
  private String password;
  private String error;
  
  public String getLogin() {
    return login;
  }
  public void setLogin(String login) {
    this.login = login;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  
  @Override
  public String toString() {
    return "LoginForm [login=" + login + ", password=" + password + "]";
  }
  public String getError() {
    return error;
  }
  public void setError(String error) {
    this.error = error;
  }
}
