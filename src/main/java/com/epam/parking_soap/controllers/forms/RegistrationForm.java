package com.epam.parking_soap.controllers.forms;

import javax.validation.constraints.Size;

import com.epam.parking_soap.repository.dao.entity.Client;

public class RegistrationForm {
  @Size(min=1, message="Login is empty")
  private String login;
  @Size(min=1, message="Password is empty")
  private String password;
  @Size(min=1, message="First name is empty")
  private String firstName;
  @Size(min=1, message="Last name is empty")
  private String lastName;
  @Size(min=1, message="Phone is empty")
  private String phone;
  private String error;
  
  public String getLogin() {
    return login;
  }
  public void setLogin(String login) {
    this.login = login;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  public String getPhone() {
    return phone;
  }
  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  public Client createClientFromForm() {
    return new Client(firstName,lastName,phone, login, password, null);
  }
  public String getError() {
    return error;
  }
  public void setError(String error) {
    this.error = error;
  }
  
}
