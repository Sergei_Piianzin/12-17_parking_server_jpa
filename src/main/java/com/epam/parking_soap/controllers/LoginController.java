package com.epam.parking_soap.controllers;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.parking_soap.controllers.forms.LoginForm;
import com.epam.parking_soap.utils.AppPaths;

@Controller
@Scope("session")
public class LoginController {
  Logger LOG = org.slf4j.LoggerFactory.getLogger(LoginController.class);

  @Autowired
  RestTemplate restTemplate;

  public LoginController() {
    LOG.debug("Create an instance of LoginController class.");
  }

  @RequestMapping(path = AppPaths.LOGIN_URL, method = RequestMethod.GET)
  public ModelAndView getLogin(ModelMap model) {

    ModelAndView mav = new ModelAndView(AppPaths.LOGIN);
    if (model.get("loginForm") == null) {
      LoginForm loginForm = new LoginForm();
      mav.addObject("loginForm", loginForm);
    }
    
    String msg = (String) model.get("logoutMessage");
    if (msg != null) {
      mav.addObject("msg", msg);
    }

    LOG.debug("mav: {}", mav);
    return mav;
  }

  @RequestMapping(path = AppPaths.LOGIN_END)
  public String logout(RedirectAttributes redirectAttrs) {
    String logoutMessage = "You are logged out now.";
    redirectAttrs.addFlashAttribute("logoutMessage", logoutMessage);
    return "redirect:" + AppPaths.LOGIN_URL;
  }
}

