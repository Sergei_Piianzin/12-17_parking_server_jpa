package com.epam.parking_soap.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.epam.parking_soap.controllers.forms.RegistrationForm;
import com.epam.parking_soap.repository.dao.entity.Client;
import com.epam.parking_soap.utils.AppPaths;
import com.epam.parking_soap.utils.ErrorMessages;
import com.epam.parking_soap.utils.RestPaths;

@Controller
public class RegistrationController {
  private Logger LOG = LoggerFactory.getLogger(RegistrationController.class);

  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private PasswordEncoder encoder;

  public RegistrationController() {
    LOG.debug("Create an instance of RegistrationServlet class.");
  }

  @RequestMapping(path = AppPaths.REGISTRATION_URL, method = RequestMethod.GET)
  public String registration(ModelMap model) {
    RegistrationForm form = new RegistrationForm();
    model.addAttribute("registrationForm", form);
    return AppPaths.REGISTRATION;
  }

  @RequestMapping(path = AppPaths.REGISTRATION_URL, method = RequestMethod.POST)
  public String registrate(ModelMap model, @Valid RegistrationForm registrationForm,
      BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      return AppPaths.REGISTRATION;
    }

    boolean isClientExist = restTemplate.getForObject(RestPaths.GET_CLIENT_EXIST, Boolean.class,
        registrationForm.getLogin());
    if (isClientExist) {
      bindingResult.rejectValue("error", "registrationForm.error", ErrorMessages.ALREADY_EXIST);
      return AppPaths.REGISTRATION;
    }
    

    Client client = registrationForm.createClientFromForm();
    String password = encoder.encode(registrationForm.getPassword());
    client.setPassword(password);
    
    restTemplate.put(RestPaths.PUT_CLIENT, client);
    model.addAttribute("login", registrationForm.getLogin());
    LOG.debug("Password: {}", password);
    model.addAttribute("password", password);
    return "redirect:" + AppPaths.PARKING_DEFAULT_URL;
  }
}
